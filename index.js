let insert = document.getElementById("input");
// insert.innerHTML = "Hello";

// let fivePowerOf3 = Math.pow(5,3);
fivePowerOf3 = 5 ** 3;
console.log(fivePowerOf3);

let exponentArray = [
  (fivePowerOf2 = Math.pow(5, 2)),
  (fivePowerOf4 = 5 ** 4),
  (twoPowerOf2 = 2 ** 2),
];

exponentArray.forEach((result) => {
  console.log(result);
});

function squareRootChecker(number) {
  return number ** 0.5;
}

let squareRootOf4 = squareRootChecker(4);
console.log(squareRootOf4);

const sentense2 = `The result of 15 to the power of 2 is ${15 ** 2}`;
const sentense3 = `The result of 3 times 6 is ${3 * 6}`;

console.log(sentense2);
console.log(sentense3);

let array = ["Kobe", "Lebron", "Jordan"];
// let goat1 = array[0];
// console.log(goat1);
// let goat2 = array[1];
// let goat3 = array[2];

let [goat1, goat2, goat3] = array;
console.log(goat1);
console.log(goat2);
console.log(goat3);

let array2 = ["Curry", "Lillard", "Paul", "Irving"];

let [pg1, pg2, pg3, pg4] = array2;

console.log(pg1);
console.log(pg2);
console.log(pg3);
console.log(pg4);

let array3 = ["Jokic", "Embiid", "Anthony-Towns", "Gobert", "Masa"];

let [center1, , center3, , center5] = array3;
console.log(center1);
console.log(center3);
console.log(center5);

let array4 = [
  "Draco Malfoy",
  "Hermione Granger",
  "Harry Potter",
  "Ron Weasley",
];

let [, gryffindor1, gryffindor2, gryffindor3] = array4;
console.log(gryffindor1);
console.log(gryffindor2);
console.log(gryffindor3);

let pokemon = {
  name: "Blastoise",
  level: 40,
  health: 80,
};

const sentence4 = `The pokemon's name is ${pokemon.name} .`;
const sentence5 = `It is a level ${pokemon.level} pokemon.`;
const sentence6 = `It has at least ${pokemon.health} health points.`;

console.log(sentence4);
console.log(sentence5);
console.log(sentence6);

let { health, name, level, trainer } = pokemon;
console.log(health);
console.log(name);
console.log(level);
console.log(trainer);

const mine = {
  name: "Paul Phoenix",
  age: 31,
  birthday: "January 12, 1991",
};

function greet(obj) {
  let { name, age, birthday } = obj;
  console.log(`Hi! My name is ${name}`);
  console.log(`I am ${age} years old.`);
  console.log(`My birthday is on ${birthday}`);
}

greet(mine);

let actors = [
  "Tom Hanks",
  "Leonardo DiCarpio",
  "Anthony Hopkins",
  "Ryan Reynolds",
];

let director = {
  name: "Alfred Hitchcock",
  birthday: "August 13, 1889",
  isActive: false,
  movies: ["The Birds", "Psycho", "North by Northwest"],
};

let [actor1, actor2, actor3, actor4] = actors;

function displayDirector(director) {
  let { name, birthday, movies, nationality } = director;
  console.log(`The Director's name is ${name}`);
  console.log(`He was born on ${birthday} in ${nationality}`);
  console.log(`His Movies include:`);
  console.log(movies);
}
console.log(actor1);
console.log(actor2);
console.log(actor3);
console.log(actor4);

displayDirector(director);

const person = { name: "masahiro kaga" };

// function greeting(personParams) {
//   console.log(`Hi, $(personParams.name}!`);
// }
const greeting = (personParams) => {
  console.log(`Hi, ${personParams.name}!`);
};

greeting(person);

let numberArr = [1, 2, 3, 4, 5];

// let multipliedBy5 = numberArr.map(function (number) {
//   return number * 5;
// });

let multipliedBy5 = numberArr.map((number) => {
  return number * 5;
});

console.log(multipliedBy5);

let multipliedBy6 = numberArr.map((number) => number * 5);
console.log(multipliedBy6);
let multipliedBy7 = numberArr.map((number) => {
  return number * 5;
});
console.log(multipliedBy7);
// let multipliedBy8 = numberArr.map(number=>{number*5});
// console.log(multipliedBy8);  undefined

const addNum = (num1, num2) => num1 + num2;
let sumExample = addNum(50, 10);
console.log(sumExample);

let obj = {
  name: "something",
  occupation: "anything",
  greet: function () {
    console.log(this);
  },
  introduceJobA: () => {
    console.log(this);
  },
  introduceJobB: function () {
    console.log(this);
  },
};

obj.greet();
obj.introduceJobA();
obj.introduceJobB();

// function Pokemon(name, type, level) {
//   this.name = name;
//   this.type = type;
//   this.level = level;
// }
//   let pokemon1 = new Pokemon("Sandshrew", "Ground", 15);
//   console.log(pokemon1);

class Pokemon {
  constructor(name, type, level) {
    this.name = name;
    this.type = type;
    this.level = level;
  }
}
let pokemon1 = new Pokemon("Sandshrew", "Ground", 15);
console.log(pokemon1);

class Car {
  constructor(brand, name, year) {
    this.brand = brand;
    this.name = name;
    this.year = year;
  }
}
let car1 = new Car("Toyota", "Vios", "2002");
console.log(car1);


